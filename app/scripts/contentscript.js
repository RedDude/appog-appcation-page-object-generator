'use strict';

var selectorsKeys = [":input:not([type=hidden])", "img", ".ui-pg-button"];
String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};
/* Inform the backgrund page that 
 * this tab should have a page-action */
chrome.runtime.sendMessage({
    from:    'content',
    subject: 'findPage'
});

/* Listen for message from the popup */
chrome.runtime.onMessage.addListener(function(msg, sender, response) {
    /* First, validate the message's structure */
    if ((msg.from === 'popup') && (msg.subject === 'DOMInfo')) {

		var pageObject = findPageObject();

        var domInfo = {
            total:   document.querySelectorAll('*').length,
            inputs:  document.querySelectorAll('input').length,
            buttons: document.querySelectorAll('button').length,
			pageObject : pageObject
        };
		

        /* Directly respond to the sender (popup), 
         * through the specified callback */
        response(domInfo);
    }
});

function findPageObject(){
 var pageObject = {};
		 pageObject.fields = [];
		 var name = window.location.pathname.split('/');
		 pageObject.className = name[name.length - 1];
		 
		 selectorsKeys.forEach(function(key) {
			 $(key).each(function(i, e){
				
				var ident = !!e.id ? e.id : !!e.name ? e.name : null;

				if(ident){
					var nameFormmated = _.camelCase(_.capitalize(ident));
		
					$(e).css('outline', 'dotted 2px red');
					var elem = {
						name: nameFormmated,
						selector : ident[0].toUpperCase() + ident.substring(1),
						type : e.localName === 'select' ? e.localName : $(e).attr('type'),
						selectorType: 'XPath'				
					}
					pageObject.fields.push(elem);
					
				}
			});
		});
	return pageObject;
}
var selectorsKeys = [":input:not([type=hidden])", "img", ".ui-pg-button"];
String.prototype.capitalize = function() {
    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};


