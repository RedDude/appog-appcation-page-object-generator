'use strict';

function setDOMInfo(info) {

	window.pageObject = info.pageObject;
	var scope = angular.element($("body")).scope();
	console.log(scope);
    scope.$apply(function(){
        scope.pageObject = info.pageObject;;
    })
		var editor = ace.edit("editor");
		editor.setTheme("ace/theme/monokai");
		editor.getSession().setMode("ace/mode/csharp"); 
		editor.setOptions({
			maxLines: 50
		});
	console.log(scope.pageObject);
    document.getElementById('total').textContent   = info.total;
    document.getElementById('inputs').textContent  = info.inputs;
    document.getElementById('buttons').textContent = info.buttons;
}


/* Once the DOM is ready... */
window.addEventListener('DOMContentLoaded', function() {
    /* ...query for the active tab... */

    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(tabs) {
        /* ...and send a request for the DOM info... */
        chrome.tabs.sendMessage(
                tabs[0].id,
                {from: 'popup', subject: 'DOMInfo'},
                /* ...also specifying a callback to be called 
                 *    from the receiving end (content script) */
                setDOMInfo);
    });
});
console.log('pop');